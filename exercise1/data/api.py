from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import (ListDataSerializer, RetrieveDataSerializer, CreateDataSerializer,
                          PartialUpdateDataSerializer, DestroyDataSerializer)
from .displays import DisplayDataSerializer


class DataViewSet(viewsets.GenericViewSet):

    def list(self, request):
        context = {
            'serializer_class': DisplayDataSerializer
        }
        serializer = ListDataSerializer(data=request.query_params, context=context)
        serializer.is_valid(raise_exception=True)
        response = serializer.create_paginated_response()
        return Response(response)

    def retrieve(self, request, pk=None):
        context = {
            'serializer_class': DisplayDataSerializer
        }
        serializer = RetrieveDataSerializer(data={'data_id': pk}, context=context)
        serializer.is_valid(raise_exception=True)
        response = serializer.create_response()
        return Response(response)

    def create(self, request):
        context = {
            'serializer_class': DisplayDataSerializer
        }
        serializer = CreateDataSerializer(data=request.data, context=context)
        serializer.is_valid(raise_exception=True)
        response = serializer.create_response()
        return Response(response)

    def update(self, request, pk=None):
        context = {
            'serializer_class': DisplayDataSerializer
        }
        data = dict(request.data)
        data['data_id'] = pk
        serializer = PartialUpdateDataSerializer(data=data, context=context)
        serializer.is_valid(raise_exception=True)
        response = serializer.create_response()
        return Response(response)

    def destroy(self, request, pk=None):
        serializer = DestroyDataSerializer(data={'data_id': pk})
        serializer.is_valid(raise_exception=True)
        response = serializer.create_dict_response()
        return Response(response)
