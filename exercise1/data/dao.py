from exercise1.utils import get_if_exists

from .models import Data


class Dao:
    data = None

    def __init__(self, data_id=None):
        if data_id:
            self.data = self.retrieve(data_id)

    @classmethod
    def list(cls):
        return Data.objects.all()

    @classmethod
    def retrieve(cls, data_id):
        return get_if_exists(Data, pk=data_id)

    @classmethod
    def create(clc, data=None):
        name = data.get('name')
        value = data.get('value')
        data = Data(name=name, value=value)
        data.save()
        return data

    def update(self, data):
        self.data.name = data.get('name', self.data.name)
        self.data.value = data.get('value', self.data.value)
        self.data.save()
        return self.data

    @classmethod
    def destroy(self, data_id):
        data = Data.objects.filter(pk=data_id).first()
        data.delete()
        return {'destroy': True}
