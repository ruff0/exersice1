from rest_framework import serializers

from .models import Data


class DisplayDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = Data
        fields = ('id', 'name', 'value')
