from django.db import models

import uuid


class Data(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(null=False, max_length=255)
    value = models.IntegerField(null=True)
