from rest_framework import serializers

from exercise1.utils import BaseSerializer
from exercise1 import exceptions

from .dao import Dao
from .models import Data


class ListDataSerializer(BaseSerializer):
    def core(self):
        return Dao.list()


class RetrieveDataSerializer(BaseSerializer):
    data_id = serializers.UUIDField()

    def core(self):
        dao = Dao(data_id=self.validated_data.get('data_id'))
        if not dao.data:
            raise exceptions.DATA_ID_NOT_EXIST
        return dao.data


class CreateDataSerializer(BaseSerializer):
    name = serializers.CharField(required=True)
    value = serializers.IntegerField(required=False)

    class Meta:
        model = Data
        fields = ('name', 'value')

    def core(self):
        return Dao.create(self.validated_data)


class PartialUpdateDataSerializer(BaseSerializer):
    data_id = serializers.UUIDField(required=True)
    name = serializers.CharField(required=False)
    value = serializers.IntegerField(required=False)

    class Meta:
        model = Data
        fields = ('id', 'name', 'value')

    def core(self):
        dao = Dao(data_id=self.validated_data.get('data_id'))
        if not dao.data:
            raise exceptions.DATA_ID_NOT_EXIST
        return dao.update(self.validated_data)


class DestroyDataSerializer(BaseSerializer):
    data_id = serializers.UUIDField()

    def core(self):
        dao = Dao(data_id=self.validated_data.get('data_id'))
        if not dao.data:
            raise exceptions.DATA_ID_NOT_EXIST
        return dao.destroy(self.validated_data.get('data_id'))
