from rest_framework import routers
from .api import DataViewSet

router = routers.SimpleRouter()
# router.trailing_slash = '/?'
router.register(r'^', DataViewSet, base_name='data_v1')

api_urlpatterns = router.urls
