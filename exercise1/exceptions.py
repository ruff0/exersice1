from rest_framework.exceptions import APIException
from rest_framework import status

from exercise1.utils import create_response


class BASE_EXCEPTION(APIException):
    status_code = status.HTTP_200_OK
    default_detail = "Base exception"
    default_code = "00000"
    message = None

    def __init__(self, detail=None, code=None):
        self.detail = create_response(None, False, detail, code)

####################################################################################################
# # # # # # # # # # # # # # # # # # # # # # # # DATA # # # # # # # # # # # # # # # # # # # # # # # #


class DATA_ID_NOT_EXIST(BASE_EXCEPTION):

    def __init__(self):
        detail = "data_id no existe"
        code = 45239
        super(DATA_ID_NOT_EXIST, self).__init__(detail, code)


class DATA_NAME_ALREADY_EXISTS(BASE_EXCEPTION):

    def __init__(self):
        detail = "ya existe un data con ese name"
        code = 29896
        super(DATA_NAME_ALREADY_EXISTS, self).__init__(detail, code)
