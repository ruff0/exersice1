from abc import abstractmethod
from django.conf import settings
from django.core.paginator import Paginator
from django.db.models.query import QuerySet

from rest_framework import serializers

PAGINATE_ELEMENTS = getattr(settings, 'PAGINATE_ELEMENTS', 10)

DEBUG = getattr(settings, 'DEBUG', False)


def create_response(data: dict=None, status: bool=True,  message: str=None, code: int=0):
    response = dict()
    if data:
        response['data'] = data
    else:
        response['data'] = None
    if message and DEBUG:
        response['message'] = message
    else:
        response['message'] = None
    response['status'] = status
    response['code'] = code
    return response


class Pagination(Paginator):
    page_elements = PAGINATE_ELEMENTS
    kwargs = {}
    serializer = None

    def __init__(self, objects, *args, **kwargs):
        self.page_elements = kwargs.get('page_elements') if kwargs.get('page_elements') else self.page_elements
        self.serializer = kwargs.get('serializer_class')
        super(Pagination, self).__init__(objects, self.page_elements)
        self.kwargs = kwargs

    def page(self, page_number):
        page_number = 1 if page_number <= 0 else super().num_pages if page_number > super().num_pages else page_number
        page = super().page(page_number)
        data = {
            'count': super().count,
            'pages': super().num_pages,
            'page_elements': self.page_elements,
            'page': page_number,
            'previous': page.previous_page_number() if page.has_previous() else None,
            'next': page.next_page_number() if page.has_next() else None,
            'items': self.serializer(list(page), many=True).data,
        }
        return data


class BaseSerializer(serializers.Serializer):
    page = serializers.IntegerField(required=False, default=1)
    page_elements = serializers.IntegerField(required=False)

    def create_paginated_response(self):
        data = self.core()
        serializer_class = self.context.get('serializer_class')
        paginator = Pagination(
            data,
            page_elements=self.data.get('page_elements'),
            serializer_class=serializer_class
        )
        return create_response(paginator.page(self.data.get('page')), True)

    def create_response(self):
        data = self.core()
        serializer_class = self.context.get('serializer_class')
        result = serializer_class(data, many=isinstance(data, QuerySet)).data
        return create_response(result, True)

    def create_dict_response(self):
        data = self.core()
        return create_response(data, True)

    @abstractmethod
    def core(self): raise NotImplementedError


def get_if_exists(model, pk):
    try:
        obj = model.objects.get(pk=pk)
    except model.DoesNotExist:
        obj = None
    return obj
